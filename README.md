# haskell-books

Haskell books to work through (cloned from https://github.com/barrymoo/chiroptical -- https://www.twitch.tv/chiroptical) 

## Books

- [ ] [Learn you a Haskell for Great Good](http://learnyouahaskell.com/)
- [ ] [Haskell Programming from First Principles](http://haskellbook.com/)
- [ ] [Thinking with Types](https://leanpub.com/thinking-with-types/)
- [ ] [Real World Haskell](http://book.realworldhaskell.org/)
- [ ] [Purely Functional Data Structures](https://www.amazon.com/Purely-Functional-Data-Structures-Okasaki/dp/0521663504)
- [ ] [Parallel and Concurrent Programming in Haskell](https://simonmar.github.io/pages/pcph.html)
- [ ] [Finding Success and Failure in Haskell](https://leanpub.com/finding-success-in-haskell)
- [ ] [Haskell in Depth](https://www.manning.com/books/haskell-in-depth) 
- [ ] [Intermediate Haskell](https://intermediatehaskell.com/)

## Courses

- [ ] [Data 61 FP Course](https://github.com/data61/fp-course)
- [ ] [Data61 Applied FP Course](https://github.com/qfpl/applied-fp-course)

## Topics

- [ ] [Data61 Lens Tutorial](https://github.com/data61/lets-lens)
- [ ] `DerivingVia`
- [ ] Nix
- [ ] Free/Freer Monads
- [ ] Tagless Final
- [ ] Algorithms
- [ ] Web API (Scotty/Servant)
- [ ] Databases (Probably HDBC)
- [ ] Recursion Schemes
  - Hylomorphism: http://www.cs.ox.ac.uk/people/nicolas.wu/papers/Hylomorphisms.pdf

## Category Theory

- https://www.youtube.com/user/MathProofsable